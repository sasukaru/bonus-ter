package ter.bonus.internal;

import java.util.Properties;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.service.util.AbstractCyActivator;
import org.osgi.framework.BundleContext;

/**
 * Cette classe est au coeur de notre
 * on y enregitre les éléments en tant que service
 * grace au contexte que l'on récupère dans la méthode 
 * start 
 * 
 * @see CyActivator#start(BundleContext)
 * 
 * dois obligatoirement hériter de {@link AbstractCyActivator}
 * @see AbstractCyActivator
 *
 * @author sasukaru
 */
public class CyActivator extends AbstractCyActivator {

	/**
	 * Permet d'activer notre app
	 * 
	 * @param context
	 * 			esentiel pour interagir avec les service, en enregister ou en recupéré
	 */
	public void start(BundleContext context) throws Exception {
		
		//grace au context on recupere le service qui nous permetra de recupéré les donné et de géré l'aplication
		CyApplicationManager cyApplicationManager = getService(context, CyApplicationManager.class);
		
		//on crée un panel avec en atribut le manager pour a l'intérieur du panel géré l'aplciation
		MyPanel mp = new MyPanel(cyApplicationManager);
		
		//on enregistre le panel et toute ses interface lié a cytoscape en une ligne 
		//on aura put pour chaque interface faire une ligne et suivre la doc et faire plein de ligne
		//mais autant utilise uen autre fonction et tout enregisté et pas se faire chier
		registerAllServices(context, mp , new Properties());

	}

}
