package ter.bonus.internal;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyRow;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;


public class MyPanel extends JPanel implements CytoPanelComponent ,ActionListener {
	/* ************************** */
	/*           atribut          */
	/* ************************** */
	
	/**
	 * Permet d'interagir avec l'app 
	 * 
	 * on le recupere via le contructeur
	 * que j'ai crée
	 */
	private CyApplicationManager m_manager;
	
	/**
	 * Sert a initialiser le panel avec les checkbox
	 */
	private JButton  m_bouton1;
	
	/**
	 * Sert a visualiser
	 */
	private JButton  m_bouton2;
	
	/**
	 * Crer un label et uen checkbox par atribut de la node table
	 */
	private JPanel   m_panelChekBox;
	
	/**
	 * Panel lié aux données du noeud selectioner
	 */
	private JPanel 	m_panelData;
	
	/**
	 * Liste les checkbox
	 */
	private List<JCheckBox> m_listCheckBox;
	
	/* ************************** */
	/*          Constructeur      */
	/* ************************** */
	public MyPanel(CyApplicationManager manager){
		super();
		
		this.m_manager = manager;
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		//initialisation des bouton
		m_bouton1 = new JButton("Initialiser");
		m_bouton2 = new JButton("visualiser");
		
		//pour pouvoir interagir avec
		m_bouton1.addActionListener(this);
		m_bouton2.addActionListener(this);
		
		this.add(m_bouton1);
		this.add(m_bouton2);
		
		//creation du panel des checkbox;
		m_panelChekBox = new JPanel();
		
		Border cadre1 = BorderFactory.createTitledBorder("Node atribute");
		
		m_panelChekBox.setBorder(cadre1);
		
		m_panelChekBox.setLayout(new BoxLayout(m_panelChekBox, BoxLayout.PAGE_AXIS));
		
		//on l'ajoute a notre panel principale
		this.add(m_panelChekBox);
		
		//creation du panel des données;
		m_panelData = new JPanel();
		
		Border cadre2 = BorderFactory.createTitledBorder("Data selectionée");
		
		m_panelData.setBorder(cadre2);
		
		m_panelData.setLayout(new BoxLayout(m_panelData , BoxLayout.PAGE_AXIS));
		
		//on l'ajoute a notre panel principale
		this.add(m_panelData );
		
		
	}
	
	/* ********************************************************** */
	/*          4  methode de l'interface CytoPanelComponent      */
	/* ********************************************************** */
	@Override
	public Component getComponent() {
		return this;
	}

	@Override
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.WEST;
	}

	@Override
	public Icon getIcon() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTitle() {
		return "app bonus";
	}

	/* ********************************************************** */
	/*    gestion des bouton, pour l'interface ActionListener     */
	/* ********************************************************** */
	public void actionPerformed(ActionEvent e) {
		
		/*
		 *Traitement destiner a crée la fenetre avec les label et checkbox
		 */
		if(e.getSource() == m_bouton1 )
		{
			//on vide le panel
			m_panelChekBox.removeAll();
			
			//on crée une liste de checkbox
			List<JCheckBox>  listtmp = new ArrayList<JCheckBox>();
			
			//on parcour la table des noeud du network actuel
			for (CyColumn colonne : m_manager.getCurrentNetwork().getDefaultNodeTable().getColumns())
			{
				//on recupere le nom
				String nom = colonne.getName();
				
				//on crée un label si atribut de la table des noeud
				if( (nom != "SUID") && (nom != "shared name") && (nom != "name") && (nom != "selected"))
				{
					//on crée un panel
					JPanel paneltmp = new JPanel();
					
					//on crée une checkbox
					JCheckBox checktmp = new JCheckBox();
					
					//on donne a la checkbox le nom de l'atribut
					checktmp.setText(nom);
					
					//on remplit le panel
					paneltmp.add(checktmp);
					
					//on ajoute le label au future panel
					m_panelChekBox.add(paneltmp);
					
					//on l'ajoute a notre liste de checkbox temporaire
					listtmp.add(checktmp);
				}
				
				//on change de liste de checkbox
				m_listCheckBox = listtmp;
			}
			
			this.updateUI();
		}
		
		/*
		 * Traitement destiner a avoir qu'un noeud selectioner et recup ses infos
		 */
		if(e.getSource() == m_bouton2 )
		{
			//on vide le panel
			m_panelData.removeAll();
			
			//on crée uen liste vide qui contiendra toutes les ligne lié au noeud selectioner
			List<CyRow> list = new ArrayList<CyRow>();
			
			//pour toutes les lignes de la table des noeud
			for(CyRow row : m_manager.getCurrentNetwork().getDefaultNodeTable().getAllRows())
			{
				//on recupere la valeur de case ayant pour titre selected
				String valeurtmp = row.getRaw("selected").toString();
				
				if (valeurtmp == "true")
				{
					list.add(row);
				}
			}
			
			//si on a que un noeud de selectioner (on a que une ligne dans la list)
			if (list.size() == 1 )
			{
				//on prend ce noeud
				CyRow row = list.get(0);
				
				//on crée un label avec les valeur du noeud selectioner 
				JLabel labtmp = new JLabel("SUID: " + row.getRaw("SUID").toString());
				
				//on ajout se label au panel
				m_panelData.add(labtmp)	;

				//on affiche en plus dans des label les champ corespondant au atribut cocher
				//pour ca je parcour ma list de checkbox
				for(JCheckBox check: m_listCheckBox)
				{
					//si la checkbox est checked
					if (check.isSelected())
					{
						//on crée un label pour l'atribut
						JLabel labtmp2 = new JLabel(check.getText() + " est selectioné");
						
						m_panelData.add(labtmp2);
					}
				}
				
				// allier row avec le text de la list des checkbox checked pour traiter les donné
				//1:on crée une list des text des checkbox selectioner
				//2:on récupere les case lié a ses nom de collone en string
				//3:on met tout en double
				//4:TODO on utilise le nom de chaque atribut (nomSelectionerCheckbox) et sa valeur dans la 
				//ligne en double (nomSelectionerNodeTableDoubl) poru crée un truc
				
				//1
				List<String> nomSelectionerCheckbox = new ArrayList<String>();
				
				for(JCheckBox check: m_listCheckBox)
				{
					if (check.isSelected())
					{
						nomSelectionerCheckbox.add(check.getText());
					}
				}
				
				//2
				List<String> nomSelectionerNodeTable = new ArrayList<String>();
				
				for (String nomtpm : nomSelectionerCheckbox)
				{
					nomSelectionerNodeTable.add(row.getRaw(nomtpm).toString());
				}
				
				//3
				List<Double> nomSelectionerNodeTableDouble = new ArrayList<Double>();
				
				for (String nomtmp : nomSelectionerNodeTable )
				{
					nomSelectionerNodeTableDouble.add( Double.parseDouble(nomtmp));
				}
				
				//4
				//on recupere la taille des liste
				int tailleList = nomSelectionerNodeTableDouble.size();
				
				DefaultPieDataset dataset = new DefaultPieDataset();
				
				for (int i = 0 ; i < tailleList ; i++)
				{
					dataset.setValue(nomSelectionerCheckbox.get(i),nomSelectionerNodeTableDouble.get(i));	
				}

				//copier coller adapter
		        JFreeChart chart = ChartFactory.createPieChart("Pie Chart",dataset,true,true,false);
				
		        PiePlot plot = (PiePlot) chart.getPlot();
		        plot.setNoDataMessage("No data available");
		        plot.setCircular(false);
		        plot.setLabelGap(0.02);
		      
		        //fin copier coller adapter
		        JPanel p1 = new ChartPanel(chart);
		        
		        m_panelData.add(p1);
			}
			
			this.updateUI();
			
		}
	}
}
